package pre1;

import java.awt.Color;
import java.awt.image.BufferedImage;
import javax.swing.text.Utilities;

public class CubicInterpolant implements Interpolant {
  @Override
  public Color interpolatePixel(float px,
                                float py,
                                BufferedImage src) {
    float dx = px * src.getWidth() - 0.5f;
    float dy = py * src.getHeight() - 0.5f;
    int x = (int) Math.floor(dx);
    int y = (int) Math.floor(dy);

    float[][] rowRGBs = new float[4][3];

    for (int yy = y - 1, iy = 0; yy <= y + 2; ++yy, ++iy) {
      float[][] columnRGBs = new float[4][3];
      for (int xx = x - 1, ix = 0; xx <= x + 2; ++xx, ++ix) {
        Color c = Utilities.getColorClamped(src, xx, yy);
        c.getRGBColorComponents(columnRGBs[ix]);
      }

      for (int ci = 0; ci < 3; ++ci) {
        rowRGBs[iy][ci] = interpolate1D(columnRGBs[0][ci], columnRGBs[1][ci], columnRGBs[2][ci], columnRGBs[3][ci], dx - x);
      }
    }

    float[] rgb = new float[3];
    for (int ci = 0; ci < 3; ++ci) {
      rgb[ci] = interpolate1D(rowRGBs[0][ci], rowRGBs[1][ci], rowRGBs[2][ci], rowRGBs[3][ci], dy - y);
      if (rgb[ci] < 0.0f) {
        rgb[ci] = 0.0f;
      } else if (rgb[ci] > 1.0f) {
        rgb[ci] = 1.0f;
      }
    }

    return new Color(rgb[0], rgb[1], rgb[2]);
  }

  public float interpolate1D(float a,
                             float b,
                             float c,
                             float d,
                             float t) {
    return b + 0.5f * t * (c - a + t * (2.0f * a - 5.0f * b + 4.0f * c - d + t * (3.0f * (b - c) + d - a)));
  }
}
